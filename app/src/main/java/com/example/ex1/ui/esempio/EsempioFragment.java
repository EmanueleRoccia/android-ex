package com.example.ex1.ui.esempio;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.ex1.R;
import com.example.ex1.databinding.FragmentEsempioBinding;

public class EsempioFragment  extends Fragment {

    private FragmentEsempioBinding esempio;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        esempio = FragmentEsempioBinding.inflate(inflater, container, false);


        final TextView textView = esempio.testo;
        textView.setText(getString(R.string.esempio));

        return esempio.getRoot();
    }
}
