package com.example.ex1.ui.test;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.ex1.R;
import com.example.ex1.databinding.DemoLayoutBinding;

public class TestFragment extends Fragment {

    private DemoLayoutBinding layout;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        layout = DemoLayoutBinding.inflate(inflater, container, false);

        TextView titolo = layout.titolo;
        String text_temp = titolo.getText().toString() + " - " + getString(R.string.esempio);
        titolo.setText(text_temp);

        EditText campouno = layout.edituno;
        EditText campodue = layout.editdue;
        TextView result = layout.res;

        Button calcola = layout.bottone;
        calcola.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                float valoredue = 0;
                float valoreuno = 0;

                if (!campouno.getText().toString().equals("") && !campodue.getText().toString().equals("")) {
                    valoredue = Float.parseFloat(campodue.getText().toString());
                    valoreuno = Float.parseFloat(campouno.getText().toString());
                }

                float somma = valoreuno + valoredue;

                result.setText("" + somma);
            }
        });

        Button bt2=layout.bt2;
        bt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                campouno.setText("");
                campodue.setText("");
                result.setText("0");
            }
        });

        View root = layout.getRoot();

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        layout = null;
    }
}